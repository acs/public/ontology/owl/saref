/*
Copyright 2020 Institute for Automation of Complex Power Systems,
E.ON Energy Research Center, RWTH Aachen University

This project is licensed under either of
- Apache License, Version 2.0
- MIT License
at your option.

Apache License, Version 2.0:

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

MIT License:

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
*/

package main

import (
	"fmt"
	"os"

	"git.rwth-aachen.de/acs/public/ontology/owl/saref/pkg/ontology"
)

func main() {
	var err error
	var mod *ontology.Model
	var file *os.File

	// deserialization
	file, err = os.Open("input.ttl")
	if err != nil {
		fmt.Println(err)
		return
	}
	mod, err = ontology.NewModelFromTTL(file)
	if err != nil {
		fmt.Println(err)
		return
	}
	err = file.Close()
	if err != nil {
		fmt.Println(err)
		return
	}
	fmt.Println(mod)

	file, err = os.Create("input.dot")
	if err != nil {
		fmt.Println(err)
		return
	}
	err = mod.ToDot(file)
	if err != nil {
		fmt.Println(err)
		return
	}
	err = file.Close()
	if err != nil {
		fmt.Println(err)
		return
	}

	// manipulation
	meas, err := mod.NewSarefMeasurement("http://example.com#meas")
	if err != nil {
		fmt.Println(err)
		return
	}
	meas.SetSarefHasValue(3.14)
	dev := mod.SarefDevice("http://example.com#dev")
	dev[0].AddSarefMakesMeasurement(meas)
	pow := mod.SarefPower("http://example.com#pow")
	meas.AddSarefRelatesToProperty(pow[0])
	pow[0].AddSarefRelatesToMeasurement(meas)
	unit, err := mod.NewSarefPowerUnit("http://example.com#kilowatt")
	if err != nil {
		fmt.Println(err)
		return
	}
	meas.AddSarefIsMeasuredIn(unit)
	task := mod.SarefTask("http://example.com#task")
	err = mod.DeleteObject(task[0])
	if err != nil {
		fmt.Println(err)
		return
	}
	fmt.Println(mod)

	// serialization
	file, err = os.Create("output.json")
	if err != nil {
		fmt.Println(err)
		return
	}
	err = mod.ToJSONLD(file)
	if err != nil {
		fmt.Println(err)
		return
	}
	err = file.Close()
	if err != nil {
		fmt.Println(err)
		return
	}

	// var g *rdf.Graph
	// g = mod.ToGraph()
	// fmt.Println(g)
	// fmt.Println(g.Nodes)

	file, err = os.Create("output.dot")
	if err != nil {
		fmt.Println(err)
		return
	}
	err = mod.ToDot(file)
	if err != nil {
		fmt.Println(err)
		return
	}
	err = file.Close()
	if err != nil {
		fmt.Println(err)
		return
	}

	return
}
