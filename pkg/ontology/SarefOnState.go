/*
This file is auto-generated by OWL2Go (https://git.rwth-aachen.de/acs/public/ontology/owl/owl2go).

Copyright 2020 Institute for Automation of Complex Power Systems,
E.ON Energy Research Center, RWTH Aachen University

This project is licensed under either of
- Apache License, Version 2.0
- MIT License
at your option.

Apache License, Version 2.0:

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

MIT License:

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
*/

package ontology

import (
	"errors"
	"git.rwth-aachen.de/acs/public/ontology/owl/owl2go/pkg/rdf"
	"git.rwth-aachen.de/acs/public/ontology/owl/owl2go/pkg/owl"
	"strings"
)

// SarefOnState The state of a device that is OFF 
type SarefOnState interface {
	SarefOnOffState
	IsSarefOnState() bool // indicates base class
}

// sSarefOnState The state of a device that is OFF 
type sSarefOnState struct {
	propCommon
}

// NewSarefOnState creates a new SarefOnState
func (mod *Model) NewSarefOnState(iri string) (ret SarefOnState, err error) {
	if mod.Exist(iri) {
		err = errors.New("Resource already exists")
		return
	}
	res := &sSarefOnState{}
	pc := propCommon{iri: iri, typ: "SarefOnState", model: mod}
	res.propCommon = pc
	mod.addSarefOnState(res)
	res.makeMaps()
	ret = res
	return
}

// makeMaps creates initiliazes maps
func (res *sSarefOnState) makeMaps() {
	return
}

// addSarefOnState adds SarefOnState to model
func (mod *Model) addSarefOnState(res SarefOnState) {
	mod.mSarefOnState[res.IRI()] = res
	mod.addSarefOnOffState(res)
	return
}

// SarefOnState returns all resources with given prefix
func (mod *Model) SarefOnState(prefix string) (res []SarefOnState) {
	for i := range mod.mSarefOnState {
		if strings.HasPrefix(i, prefix) {
			res = append(res, mod.mSarefOnState[i])
		}
	}
	return
}

// RemoveObject deletes all its references in this object
func (res *sSarefOnState) RemoveObject(obj owl.Thing, prop string) {
	switch prop {
	default:
	}
	return
}

// isSarefOnState indicates base class
func (res *sSarefOnState) IsSarefOnState() bool {
	return true
}

// isSarefOnOffState indicates base class
func (res *sSarefOnState) IsSarefOnOffState() bool {
	return true
}

// isSarefState indicates base class
func (res *sSarefOnState) IsSarefState() bool {
	return true
}

// InitFromNode initializes the resource from a graph node
func (res *sSarefOnState) InitFromNode(node *rdf.Node) (err error) {
	for i := range node.Edge {
		res.propsInit(node.Edge[i])
	}
	return
}

// propsInit initializes the property from a graph node
func (res *sSarefOnState) propsInit(pred *rdf.Edge) (err error) {
	switch pred.Pred.String() {
	default:
	}
	return
}

// ToGraph creates a new owl graph node and adds it to the graph
func (res *sSarefOnState) ToGraph(g *rdf.Graph) {
	owl.AddObjectToGraph(g, "https://w3id.org/saref#OnState", res)
	return
}

// String prints the object into a string
func (res *sSarefOnState) String() (ret string) {
	ret = res.iri + " " + "SarefOnState\n"
	ret += res.propsString()
	return
}

// propsString prints the object into a string
func (res *sSarefOnState) propsString() (ret string) {
	ret = ""
	return
}


