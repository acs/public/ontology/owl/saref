/*
This file is auto-generated by OWL2Go (https://git.rwth-aachen.de/acs/public/ontology/owl/owl2go).

Copyright 2020 Institute for Automation of Complex Power Systems,
E.ON Energy Research Center, RWTH Aachen University

This project is licensed under either of
- Apache License, Version 2.0
- MIT License
at your option.

Apache License, Version 2.0:

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

MIT License:

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
*/

package ontology

import (
	"errors"
	"git.rwth-aachen.de/acs/public/ontology/owl/owl2go/pkg/rdf"
	"git.rwth-aachen.de/acs/public/ontology/owl/owl2go/pkg/owl"
	"strings"
)

// TimeDateTimeDescription Description of date and time structured with separate values for the various elements of a calendar-clock system. The temporal reference system is fixed to Gregorian Calendar, and the range of year, month, day properties restricted to corresponding XML Schema types xsd:gYear, xsd:gMonth and xsd:gDay, respectively.
type TimeDateTimeDescription interface {
	TimeGeneralDateTimeDescription
	IsTimeDateTimeDescription() bool // indicates base class
}

// sTimeDateTimeDescription Description of date and time structured with separate values for the various elements of a calendar-clock system. The temporal reference system is fixed to Gregorian Calendar, and the range of year, month, day properties restricted to corresponding XML Schema types xsd:gYear, xsd:gMonth and xsd:gDay, respectively.
type sTimeDateTimeDescription struct {
	sTimeGeneralDateTimeDescription
}

// NewTimeDateTimeDescription creates a new TimeDateTimeDescription
func (mod *Model) NewTimeDateTimeDescription(iri string) (ret TimeDateTimeDescription, err error) {
	if mod.Exist(iri) {
		err = errors.New("Resource already exists")
		return
	}
	res := &sTimeDateTimeDescription{}
	pc := propCommon{iri: iri, typ: "TimeDateTimeDescription", model: mod}
	res.propCommon = pc
	mod.addTimeDateTimeDescription(res)
	res.makeMaps()
	ret = res
	return
}

// addTimeDateTimeDescription adds TimeDateTimeDescription to model
func (mod *Model) addTimeDateTimeDescription(res TimeDateTimeDescription) {
	mod.mTimeDateTimeDescription[res.IRI()] = res
	mod.addTimeGeneralDateTimeDescription(res)
	return
}

// TimeDateTimeDescription returns all resources with given prefix
func (mod *Model) TimeDateTimeDescription(prefix string) (res []TimeDateTimeDescription) {
	for i := range mod.mTimeDateTimeDescription {
		if strings.HasPrefix(i, prefix) {
			res = append(res, mod.mTimeDateTimeDescription[i])
		}
	}
	return
}

// isTimeDateTimeDescription indicates base class
func (res *sTimeDateTimeDescription) IsTimeDateTimeDescription() bool {
	return true
}

// ToGraph creates a new owl graph node and adds it to the graph
func (res *sTimeDateTimeDescription) ToGraph(g *rdf.Graph) {
	node := owl.AddObjectToGraph(g, "http://www.w3.org/2006/time#DateTimeDescription", res)
	res.propsToGraph(node, g)
	return
}

// String prints the object into a string
func (res *sTimeDateTimeDescription) String() (ret string) {
	ret = res.iri + " " + "TimeDateTimeDescription\n"
	ret += res.propsString()
	return
}


