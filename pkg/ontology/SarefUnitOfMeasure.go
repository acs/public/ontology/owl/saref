/*
This file is auto-generated by OWL2Go (https://git.rwth-aachen.de/acs/public/ontology/owl/owl2go).

Copyright 2020 Institute for Automation of Complex Power Systems,
E.ON Energy Research Center, RWTH Aachen University

This project is licensed under either of
- Apache License, Version 2.0
- MIT License
at your option.

Apache License, Version 2.0:

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

MIT License:

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
*/

package ontology

import (
	"errors"
	"git.rwth-aachen.de/acs/public/ontology/owl/owl2go/pkg/rdf"
	"git.rwth-aachen.de/acs/public/ontology/owl/owl2go/pkg/owl"
	"strings"
)

// SarefUnitOfMeasure The unit of measure is a standard for measurement of a quantity, such as a Property. For example,  Power is a property and Watt is a unit of power that represents a definite predetermined power: when we say 10 Watt, we actually mean 10 times the definite predetermined power called \"watt\". Our definition is based on the definition of unit of measure in the Ontology of units of Measure (OM). We propose here a list of some units of measure that are relevant for the purpose of SAREF, but this list can be extended, also using some other ontologies rather than the Ontology of units of Measure (OM).
type SarefUnitOfMeasure interface {
	owl.Thing
	IsSarefUnitOfMeasure() bool // indicates base class
}

// sSarefUnitOfMeasure The unit of measure is a standard for measurement of a quantity, such as a Property. For example,  Power is a property and Watt is a unit of power that represents a definite predetermined power: when we say 10 Watt, we actually mean 10 times the definite predetermined power called \"watt\". Our definition is based on the definition of unit of measure in the Ontology of units of Measure (OM). We propose here a list of some units of measure that are relevant for the purpose of SAREF, but this list can be extended, also using some other ontologies rather than the Ontology of units of Measure (OM).
type sSarefUnitOfMeasure struct {
	propCommon
}

// NewSarefUnitOfMeasure creates a new SarefUnitOfMeasure
func (mod *Model) NewSarefUnitOfMeasure(iri string) (ret SarefUnitOfMeasure, err error) {
	if mod.Exist(iri) {
		err = errors.New("Resource already exists")
		return
	}
	res := &sSarefUnitOfMeasure{}
	pc := propCommon{iri: iri, typ: "SarefUnitOfMeasure", model: mod}
	res.propCommon = pc
	mod.addSarefUnitOfMeasure(res)
	res.makeMaps()
	ret = res
	return
}

// makeMaps creates initiliazes maps
func (res *sSarefUnitOfMeasure) makeMaps() {
	return
}

// addSarefUnitOfMeasure adds SarefUnitOfMeasure to model
func (mod *Model) addSarefUnitOfMeasure(res SarefUnitOfMeasure) {
	mod.mSarefUnitOfMeasure[res.IRI()] = res
	mod.mThing[res.IRI()] = res
	return
}

// SarefUnitOfMeasure returns all resources with given prefix
func (mod *Model) SarefUnitOfMeasure(prefix string) (res []SarefUnitOfMeasure) {
	for i := range mod.mSarefUnitOfMeasure {
		if strings.HasPrefix(i, prefix) {
			res = append(res, mod.mSarefUnitOfMeasure[i])
		}
	}
	return
}

// RemoveObject deletes all its references in this object
func (res *sSarefUnitOfMeasure) RemoveObject(obj owl.Thing, prop string) {
	switch prop {
	default:
	}
	return
}

// isSarefUnitOfMeasure indicates base class
func (res *sSarefUnitOfMeasure) IsSarefUnitOfMeasure() bool {
	return true
}

// InitFromNode initializes the resource from a graph node
func (res *sSarefUnitOfMeasure) InitFromNode(node *rdf.Node) (err error) {
	for i := range node.Edge {
		res.propsInit(node.Edge[i])
	}
	return
}

// propsInit initializes the property from a graph node
func (res *sSarefUnitOfMeasure) propsInit(pred *rdf.Edge) (err error) {
	switch pred.Pred.String() {
	default:
	}
	return
}

// ToGraph creates a new owl graph node and adds it to the graph
func (res *sSarefUnitOfMeasure) ToGraph(g *rdf.Graph) {
	owl.AddObjectToGraph(g, "https://w3id.org/saref#UnitOfMeasure", res)
	return
}

// String prints the object into a string
func (res *sSarefUnitOfMeasure) String() (ret string) {
	ret = res.iri + " " + "SarefUnitOfMeasure\n"
	ret += res.propsString()
	return
}

// propsString prints the object into a string
func (res *sSarefUnitOfMeasure) propsString() (ret string) {
	ret = ""
	return
}


