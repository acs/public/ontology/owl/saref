/*
This file is auto-generated by OWL2Go (https://git.rwth-aachen.de/acs/public/ontology/owl/owl2go).

Copyright 2020 Institute for Automation of Complex Power Systems,
E.ON Energy Research Center, RWTH Aachen University

This project is licensed under either of
- Apache License, Version 2.0
- MIT License
at your option.

Apache License, Version 2.0:

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

MIT License:

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
*/

package ontology

import (
	"errors"
	"git.rwth-aachen.de/acs/public/ontology/owl/owl2go/pkg/rdf"
	"git.rwth-aachen.de/acs/public/ontology/owl/owl2go/pkg/owl"
	"strings"
)

// SarefTemperature A saref:Property related to some measurements that are characterized by a certain value that is measured in a temperature unit (degree_Celsius, degree_Fahrenheit, or degree_kelvin)
type SarefTemperature interface {
	SarefProperty
	IsSarefTemperature() bool // indicates base class
}

// sSarefTemperature A saref:Property related to some measurements that are characterized by a certain value that is measured in a temperature unit (degree_Celsius, degree_Fahrenheit, or degree_kelvin)
type sSarefTemperature struct {
	sSarefProperty
}

// NewSarefTemperature creates a new SarefTemperature
func (mod *Model) NewSarefTemperature(iri string) (ret SarefTemperature, err error) {
	if mod.Exist(iri) {
		err = errors.New("Resource already exists")
		return
	}
	res := &sSarefTemperature{}
	pc := propCommon{iri: iri, typ: "SarefTemperature", model: mod}
	res.propCommon = pc
	mod.addSarefTemperature(res)
	res.makeMaps()
	ret = res
	return
}

// addSarefTemperature adds SarefTemperature to model
func (mod *Model) addSarefTemperature(res SarefTemperature) {
	mod.mSarefTemperature[res.IRI()] = res
	mod.addSarefProperty(res)
	return
}

// SarefTemperature returns all resources with given prefix
func (mod *Model) SarefTemperature(prefix string) (res []SarefTemperature) {
	for i := range mod.mSarefTemperature {
		if strings.HasPrefix(i, prefix) {
			res = append(res, mod.mSarefTemperature[i])
		}
	}
	return
}

// isSarefTemperature indicates base class
func (res *sSarefTemperature) IsSarefTemperature() bool {
	return true
}

// ToGraph creates a new owl graph node and adds it to the graph
func (res *sSarefTemperature) ToGraph(g *rdf.Graph) {
	node := owl.AddObjectToGraph(g, "https://w3id.org/saref#Temperature", res)
	res.propsToGraph(node, g)
	return
}

// String prints the object into a string
func (res *sSarefTemperature) String() (ret string) {
	ret = res.iri + " " + "SarefTemperature\n"
	ret += res.propsString()
	return
}


