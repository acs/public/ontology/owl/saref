/*
This file is auto-generated by OWL2Go (https://git.rwth-aachen.de/acs/public/ontology/owl/owl2go).

Copyright 2020 Institute for Automation of Complex Power Systems,
E.ON Energy Research Center, RWTH Aachen University

This project is licensed under either of
- Apache License, Version 2.0
- MIT License
at your option.

Apache License, Version 2.0:

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

MIT License:

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
*/

package ontology

import (
	"errors"
	"git.rwth-aachen.de/acs/public/ontology/owl/owl2go/pkg/rdf"
	"git.rwth-aachen.de/acs/public/ontology/owl/owl2go/pkg/owl"
	"strings"
)

// SarefStartCommand A type of command
type SarefStartCommand interface {
	SarefCommand
	IsSarefStartCommand() bool // indicates base class
}

// sSarefStartCommand A type of command
type sSarefStartCommand struct {
	propCommon
	propSarefActsUponBaseSarefStateTypeSarefStartStopStateMultipleSarefStartStopState
	propSarefIsCommandOfBaseSarefFunctionTypeSarefFunctionMultipleSarefFunction
	propSarefHasDescriptionBasestringTypestringSinglestring
}

// NewSarefStartCommand creates a new SarefStartCommand
func (mod *Model) NewSarefStartCommand(iri string) (ret SarefStartCommand, err error) {
	if mod.Exist(iri) {
		err = errors.New("Resource already exists")
		return
	}
	res := &sSarefStartCommand{}
	pc := propCommon{iri: iri, typ: "SarefStartCommand", model: mod}
	res.propCommon = pc
	mod.addSarefStartCommand(res)
	res.makeMaps()
	ret = res
	return
}

// makeMaps creates initiliazes maps
func (res *sSarefStartCommand) makeMaps() {
	res.sarefActsUpon = make(map[string]SarefStartStopState)
	res.sarefIsCommandOf = make(map[string]SarefFunction)
	return
}

// addSarefStartCommand adds SarefStartCommand to model
func (mod *Model) addSarefStartCommand(res SarefStartCommand) {
	mod.mSarefStartCommand[res.IRI()] = res
	mod.addSarefCommand(res)
	return
}

// SarefStartCommand returns all resources with given prefix
func (mod *Model) SarefStartCommand(prefix string) (res []SarefStartCommand) {
	for i := range mod.mSarefStartCommand {
		if strings.HasPrefix(i, prefix) {
			res = append(res, mod.mSarefStartCommand[i])
		}
	}
	return
}

// RemoveObject deletes all its references in this object
func (res *sSarefStartCommand) RemoveObject(obj owl.Thing, prop string) {
	switch prop {
	case "https://w3id.org/saref#actsUpon":
		res.propSarefActsUponBaseSarefStateTypeSarefStartStopStateMultipleSarefStartStopState.removeObject(obj)
	case "https://w3id.org/saref#isCommandOf":
		if v, ok := obj.(SarefFunction); ok {
			res.DelSarefIsCommandOf(v)
		}
	default:
	}
	return
}

// isSarefStartCommand indicates base class
func (res *sSarefStartCommand) IsSarefStartCommand() bool {
	return true
}

// isSarefCommand indicates base class
func (res *sSarefStartCommand) IsSarefCommand() bool {
	return true
}

// SetSarefIsCommandOf is setter of A relationship between a command and a function.
func (res *sSarefStartCommand) SetSarefIsCommandOf(in []SarefFunction) (err error) {
	temp := res.sarefIsCommandOf
	res.sarefIsCommandOf = make(map[string]SarefFunction)
	for i := range temp {
		temp[i].DelSarefHasCommand(res)
	}
	err = res.AddSarefIsCommandOf(in...)
	return
}

// AddSarefIsCommandOf adds A relationship between a command and a function.
func (res *sSarefStartCommand) AddSarefIsCommandOf(in ...SarefFunction) (err error) {
	for i := range in {
		if _, ok := res.sarefIsCommandOf[in[i].IRI()]; !ok {
			res.sarefIsCommandOf[in[i].IRI()] = in[i]
			in[i].AddSarefHasCommand(res)
		}
	}
	return
}

// DelSarefIsCommandOf deletes A relationship between a command and a function.
func (res *sSarefStartCommand) DelSarefIsCommandOf(in ...SarefFunction) (err error) {
	for i := range in {
		if _, ok := res.sarefIsCommandOf[in[i].IRI()]; ok {
			delete(res.sarefIsCommandOf, in[i].IRI())
			in[i].DelSarefHasCommand(res)
		}
	}
	return
}

// InitFromNode initializes the resource from a graph node
func (res *sSarefStartCommand) InitFromNode(node *rdf.Node) (err error) {
	for i := range node.Edge {
		res.propsInit(node.Edge[i])
	}
	return
}

// propsInit initializes the property from a graph node
func (res *sSarefStartCommand) propsInit(pred *rdf.Edge) (err error) {
	switch pred.Pred.String() {
	case "https://w3id.org/saref#actsUpon":
		res.propSarefActsUponBaseSarefStateTypeSarefStartStopStateMultipleSarefStartStopState.init(res.model, pred.Object.Term.String())
	case "https://w3id.org/saref#isCommandOf":
		if obj, ok := res.model.mSarefFunction[pred.Object.Term.String()]; ok {
			res.AddSarefIsCommandOf(obj)
		}
	case "https://w3id.org/saref#hasDescription":
		res.propSarefHasDescriptionBasestringTypestringSinglestring.init(pred.Object.Term.String())
	default:
	}
	return
}

// ToGraph creates a new owl graph node and adds it to the graph
func (res *sSarefStartCommand) ToGraph(g *rdf.Graph) {
	node := owl.AddObjectToGraph(g, "https://w3id.org/saref#StartCommand", res)
	res.propsToGraph(node, g)
	return
}

// propsToGraph adds all properties to the graph
func (res *sSarefStartCommand) propsToGraph(node *rdf.Node, g *rdf.Graph) {
	res.propSarefActsUponBaseSarefStateTypeSarefStartStopStateMultipleSarefStartStopState.toGraph(node, g)
	res.propSarefIsCommandOfBaseSarefFunctionTypeSarefFunctionMultipleSarefFunction.toGraph(node, g)
	res.propSarefHasDescriptionBasestringTypestringSinglestring.toGraph(node, g)
	return
}

// String prints the object into a string
func (res *sSarefStartCommand) String() (ret string) {
	ret = res.iri + " " + "SarefStartCommand\n"
	ret += res.propsString()
	return
}

// propsString prints the object into a string
func (res *sSarefStartCommand) propsString() (ret string) {
	ret = ""
	ret += res.propSarefActsUponBaseSarefStateTypeSarefStartStopStateMultipleSarefStartStopState.String()
	ret += res.propSarefIsCommandOfBaseSarefFunctionTypeSarefFunctionMultipleSarefFunction.String()
	ret += res.propSarefHasDescriptionBasestringTypestringSinglestring.String()
	return
}


