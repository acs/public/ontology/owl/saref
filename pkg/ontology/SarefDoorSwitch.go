/*
This file is auto-generated by OWL2Go (https://git.rwth-aachen.de/acs/public/ontology/owl/owl2go).

Copyright 2020 Institute for Automation of Complex Power Systems,
E.ON Energy Research Center, RWTH Aachen University

This project is licensed under either of
- Apache License, Version 2.0
- MIT License
at your option.

Apache License, Version 2.0:

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

MIT License:

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
*/

package ontology

import (
	"errors"
	"git.rwth-aachen.de/acs/public/ontology/owl/owl2go/pkg/rdf"
	"git.rwth-aachen.de/acs/public/ontology/owl/owl2go/pkg/owl"
	"strings"
)

// SarefDoorSwitch A device of category saref:Actuator that consists of a switch, accomplishes the task saref:Safety, performs the saref:OpenCloseFunction, is used for controlling a door, and can be found in the state saref:OpenCloseState.
type SarefDoorSwitch interface {
	SarefSwitch
	IsSarefDoorSwitch() bool // indicates base class
}

// sSarefDoorSwitch A device of category saref:Actuator that consists of a switch, accomplishes the task saref:Safety, performs the saref:OpenCloseFunction, is used for controlling a door, and can be found in the state saref:OpenCloseState.
type sSarefDoorSwitch struct {
	propCommon
	propSarefConsistsOfBaseSarefDeviceTypeSarefSwitchMultipleSarefSwitch
	propSarefControlsPropertyBaseSarefPropertyTypeSarefPropertyMultipleSarefProperty
	propSarefHasProfileBaseSarefProfileTypeSarefProfileMultipleSarefProfile
	propSarefHasStateBaseSarefStateTypeSarefOpenCloseStateMultipleSarefOpenCloseState
	propSarefHasTypicalConsumptionBaseSarefPropertyTypeSarefPropertyMultipleSarefEnergySarefPower
	propSarefIsUsedForBaseSarefCommodityTypeSarefCommodityMultipleSarefCommodity
	propSarefMakesMeasurementBaseSarefMeasurementTypeSarefMeasurementMultipleSarefMeasurement
	propSarefMeasuresPropertyBaseSarefPropertyTypeSarefPropertyMultipleSarefProperty
	propSarefOffersBaseSarefServiceTypeSarefServiceMultipleSarefService
	propSarefAccomplishesBaseSarefTaskTypeSarefTaskMultipleSarefTask
	propSarefHasFunctionBaseSarefFunctionTypeSarefOpenCloseFunctionMultipleSarefOpenCloseFunction
	propSarefHasDescriptionBasestringTypestringSinglestring
	propSarefHasManufacturerBasestringTypestringSinglestring
	propSarefHasModelBasestringTypestringSinglestring
}

// NewSarefDoorSwitch creates a new SarefDoorSwitch
func (mod *Model) NewSarefDoorSwitch(iri string) (ret SarefDoorSwitch, err error) {
	if mod.Exist(iri) {
		err = errors.New("Resource already exists")
		return
	}
	res := &sSarefDoorSwitch{}
	pc := propCommon{iri: iri, typ: "SarefDoorSwitch", model: mod}
	res.propCommon = pc
	mod.addSarefDoorSwitch(res)
	res.makeMaps()
	ret = res
	return
}

// makeMaps creates initiliazes maps
func (res *sSarefDoorSwitch) makeMaps() {
	res.sarefConsistsOf = make(map[string]SarefSwitch)
	res.sarefControlsProperty = make(map[string]SarefProperty)
	res.sarefHasProfile = make(map[string]SarefProfile)
	res.sarefHasState = make(map[string]SarefOpenCloseState)
	res.sarefHasTypicalConsumption = make(map[string]SarefProperty)
	res.sarefIsUsedFor = make(map[string]SarefCommodity)
	res.sarefMakesMeasurement = make(map[string]SarefMeasurement)
	res.sarefMeasuresProperty = make(map[string]SarefProperty)
	res.sarefOffers = make(map[string]SarefService)
	res.sarefAccomplishes = make(map[string]SarefTask)
	res.sarefHasFunction = make(map[string]SarefOpenCloseFunction)
	if v, ok := res.model.mSarefTask["https://w3id.org/saref#Safety"]; ok {
		res.AddSarefAccomplishes(v)
	}
	return
}

// addSarefDoorSwitch adds SarefDoorSwitch to model
func (mod *Model) addSarefDoorSwitch(res SarefDoorSwitch) {
	mod.mSarefDoorSwitch[res.IRI()] = res
	mod.addSarefSwitch(res)
	return
}

// SarefDoorSwitch returns all resources with given prefix
func (mod *Model) SarefDoorSwitch(prefix string) (res []SarefDoorSwitch) {
	for i := range mod.mSarefDoorSwitch {
		if strings.HasPrefix(i, prefix) {
			res = append(res, mod.mSarefDoorSwitch[i])
		}
	}
	return
}

// RemoveObject deletes all its references in this object
func (res *sSarefDoorSwitch) RemoveObject(obj owl.Thing, prop string) {
	switch prop {
	case "https://w3id.org/saref#consistsOf":
		res.propSarefConsistsOfBaseSarefDeviceTypeSarefSwitchMultipleSarefSwitch.removeObject(obj)
	case "https://w3id.org/saref#controlsProperty":
		res.propSarefControlsPropertyBaseSarefPropertyTypeSarefPropertyMultipleSarefProperty.removeObject(obj)
	case "https://w3id.org/saref#hasProfile":
		res.propSarefHasProfileBaseSarefProfileTypeSarefProfileMultipleSarefProfile.removeObject(obj)
	case "https://w3id.org/saref#hasState":
		res.propSarefHasStateBaseSarefStateTypeSarefOpenCloseStateMultipleSarefOpenCloseState.removeObject(obj)
	case "https://w3id.org/saref#hasTypicalConsumption":
		res.propSarefHasTypicalConsumptionBaseSarefPropertyTypeSarefPropertyMultipleSarefEnergySarefPower.removeObject(obj)
	case "https://w3id.org/saref#isUsedFor":
		res.propSarefIsUsedForBaseSarefCommodityTypeSarefCommodityMultipleSarefCommodity.removeObject(obj)
	case "https://w3id.org/saref#makesMeasurement":
		res.propSarefMakesMeasurementBaseSarefMeasurementTypeSarefMeasurementMultipleSarefMeasurement.removeObject(obj)
	case "https://w3id.org/saref#measuresProperty":
		res.propSarefMeasuresPropertyBaseSarefPropertyTypeSarefPropertyMultipleSarefProperty.removeObject(obj)
	case "https://w3id.org/saref#offers":
		if v, ok := obj.(SarefService); ok {
			res.DelSarefOffers(v)
		}
	case "https://w3id.org/saref#accomplishes":
		if v, ok := obj.(SarefTask); ok {
			res.DelSarefAccomplishes(v)
		}
	case "https://w3id.org/saref#hasFunction":
		res.propSarefHasFunctionBaseSarefFunctionTypeSarefOpenCloseFunctionMultipleSarefOpenCloseFunction.removeObject(obj)
	default:
	}
	return
}

// isSarefDoorSwitch indicates base class
func (res *sSarefDoorSwitch) IsSarefDoorSwitch() bool {
	return true
}

// isSarefSwitch indicates base class
func (res *sSarefDoorSwitch) IsSarefSwitch() bool {
	return true
}

// isSarefActuator indicates base class
func (res *sSarefDoorSwitch) IsSarefActuator() bool {
	return true
}

// isSarefFunctionRelated indicates base class
func (res *sSarefDoorSwitch) IsSarefFunctionRelated() bool {
	return true
}

// isSarefDevice indicates base class
func (res *sSarefDoorSwitch) IsSarefDevice() bool {
	return true
}

// SetSarefOffers is setter of A relationship between a device and a service
func (res *sSarefDoorSwitch) SetSarefOffers(in []SarefService) (err error) {
	temp := res.sarefOffers
	res.sarefOffers = make(map[string]SarefService)
	for i := range temp {
		temp[i].DelSarefIsOfferedBy(res)
	}
	err = res.AddSarefOffers(in...)
	return
}

// AddSarefOffers adds A relationship between a device and a service
func (res *sSarefDoorSwitch) AddSarefOffers(in ...SarefService) (err error) {
	for i := range in {
		if _, ok := res.sarefOffers[in[i].IRI()]; !ok {
			res.sarefOffers[in[i].IRI()] = in[i]
			in[i].AddSarefIsOfferedBy(res)
		}
	}
	return
}

// DelSarefOffers deletes A relationship between a device and a service
func (res *sSarefDoorSwitch) DelSarefOffers(in ...SarefService) (err error) {
	for i := range in {
		if _, ok := res.sarefOffers[in[i].IRI()]; ok {
			delete(res.sarefOffers, in[i].IRI())
			in[i].DelSarefIsOfferedBy(res)
		}
	}
	return
}

// SetSarefAccomplishes is setter of A relationship between a certain entity (e.g., a device) and the task it accomplishes
func (res *sSarefDoorSwitch) SetSarefAccomplishes(in []SarefTask) (err error) {
	temp := res.sarefAccomplishes
	res.sarefAccomplishes = make(map[string]SarefTask)
	for i := range temp {
		temp[i].DelSarefIsAccomplishedBy(res)
	}
	err = res.AddSarefAccomplishes(in...)
	return
}

// AddSarefAccomplishes adds A relationship between a certain entity (e.g., a device) and the task it accomplishes
func (res *sSarefDoorSwitch) AddSarefAccomplishes(in ...SarefTask) (err error) {
	for i := range in {
		if _, ok := res.sarefAccomplishes[in[i].IRI()]; !ok {
			res.sarefAccomplishes[in[i].IRI()] = in[i]
			in[i].AddSarefIsAccomplishedBy(res)
		}
	}
	return
}

// DelSarefAccomplishes deletes A relationship between a certain entity (e.g., a device) and the task it accomplishes
func (res *sSarefDoorSwitch) DelSarefAccomplishes(in ...SarefTask) (err error) {
	for i := range in {
		if _, ok := res.sarefAccomplishes[in[i].IRI()]; ok {
			delete(res.sarefAccomplishes, in[i].IRI())
			in[i].DelSarefIsAccomplishedBy(res)
		}
	}
	return
}

// InitFromNode initializes the resource from a graph node
func (res *sSarefDoorSwitch) InitFromNode(node *rdf.Node) (err error) {
	for i := range node.Edge {
		res.propsInit(node.Edge[i])
	}
	return
}

// propsInit initializes the property from a graph node
func (res *sSarefDoorSwitch) propsInit(pred *rdf.Edge) (err error) {
	switch pred.Pred.String() {
	case "https://w3id.org/saref#consistsOf":
		res.propSarefConsistsOfBaseSarefDeviceTypeSarefSwitchMultipleSarefSwitch.init(res.model, pred.Object.Term.String())
	case "https://w3id.org/saref#controlsProperty":
		res.propSarefControlsPropertyBaseSarefPropertyTypeSarefPropertyMultipleSarefProperty.init(res.model, pred.Object.Term.String())
	case "https://w3id.org/saref#hasProfile":
		res.propSarefHasProfileBaseSarefProfileTypeSarefProfileMultipleSarefProfile.init(res.model, pred.Object.Term.String())
	case "https://w3id.org/saref#hasState":
		res.propSarefHasStateBaseSarefStateTypeSarefOpenCloseStateMultipleSarefOpenCloseState.init(res.model, pred.Object.Term.String())
	case "https://w3id.org/saref#hasTypicalConsumption":
		res.propSarefHasTypicalConsumptionBaseSarefPropertyTypeSarefPropertyMultipleSarefEnergySarefPower.init(res.model, pred.Object.Term.String())
	case "https://w3id.org/saref#isUsedFor":
		res.propSarefIsUsedForBaseSarefCommodityTypeSarefCommodityMultipleSarefCommodity.init(res.model, pred.Object.Term.String())
	case "https://w3id.org/saref#makesMeasurement":
		res.propSarefMakesMeasurementBaseSarefMeasurementTypeSarefMeasurementMultipleSarefMeasurement.init(res.model, pred.Object.Term.String())
	case "https://w3id.org/saref#measuresProperty":
		res.propSarefMeasuresPropertyBaseSarefPropertyTypeSarefPropertyMultipleSarefProperty.init(res.model, pred.Object.Term.String())
	case "https://w3id.org/saref#offers":
		if obj, ok := res.model.mSarefService[pred.Object.Term.String()]; ok {
			res.AddSarefOffers(obj)
		}
	case "https://w3id.org/saref#accomplishes":
		if obj, ok := res.model.mSarefTask[pred.Object.Term.String()]; ok {
			res.AddSarefAccomplishes(obj)
		}
	case "https://w3id.org/saref#hasFunction":
		res.propSarefHasFunctionBaseSarefFunctionTypeSarefOpenCloseFunctionMultipleSarefOpenCloseFunction.init(res.model, pred.Object.Term.String())
	case "https://w3id.org/saref#hasDescription":
		res.propSarefHasDescriptionBasestringTypestringSinglestring.init(pred.Object.Term.String())
	case "https://w3id.org/saref#hasManufacturer":
		res.propSarefHasManufacturerBasestringTypestringSinglestring.init(pred.Object.Term.String())
	case "https://w3id.org/saref#hasModel":
		res.propSarefHasModelBasestringTypestringSinglestring.init(pred.Object.Term.String())
	default:
	}
	return
}

// ToGraph creates a new owl graph node and adds it to the graph
func (res *sSarefDoorSwitch) ToGraph(g *rdf.Graph) {
	node := owl.AddObjectToGraph(g, "https://w3id.org/saref#DoorSwitch", res)
	res.propsToGraph(node, g)
	return
}

// propsToGraph adds all properties to the graph
func (res *sSarefDoorSwitch) propsToGraph(node *rdf.Node, g *rdf.Graph) {
	res.propSarefConsistsOfBaseSarefDeviceTypeSarefSwitchMultipleSarefSwitch.toGraph(node, g)
	res.propSarefControlsPropertyBaseSarefPropertyTypeSarefPropertyMultipleSarefProperty.toGraph(node, g)
	res.propSarefHasProfileBaseSarefProfileTypeSarefProfileMultipleSarefProfile.toGraph(node, g)
	res.propSarefHasStateBaseSarefStateTypeSarefOpenCloseStateMultipleSarefOpenCloseState.toGraph(node, g)
	res.propSarefHasTypicalConsumptionBaseSarefPropertyTypeSarefPropertyMultipleSarefEnergySarefPower.toGraph(node, g)
	res.propSarefIsUsedForBaseSarefCommodityTypeSarefCommodityMultipleSarefCommodity.toGraph(node, g)
	res.propSarefMakesMeasurementBaseSarefMeasurementTypeSarefMeasurementMultipleSarefMeasurement.toGraph(node, g)
	res.propSarefMeasuresPropertyBaseSarefPropertyTypeSarefPropertyMultipleSarefProperty.toGraph(node, g)
	res.propSarefOffersBaseSarefServiceTypeSarefServiceMultipleSarefService.toGraph(node, g)
	res.propSarefAccomplishesBaseSarefTaskTypeSarefTaskMultipleSarefTask.toGraph(node, g)
	res.propSarefHasFunctionBaseSarefFunctionTypeSarefOpenCloseFunctionMultipleSarefOpenCloseFunction.toGraph(node, g)
	res.propSarefHasDescriptionBasestringTypestringSinglestring.toGraph(node, g)
	res.propSarefHasManufacturerBasestringTypestringSinglestring.toGraph(node, g)
	res.propSarefHasModelBasestringTypestringSinglestring.toGraph(node, g)
	return
}

// String prints the object into a string
func (res *sSarefDoorSwitch) String() (ret string) {
	ret = res.iri + " " + "SarefDoorSwitch\n"
	ret += res.propsString()
	return
}

// propsString prints the object into a string
func (res *sSarefDoorSwitch) propsString() (ret string) {
	ret = ""
	ret += res.propSarefConsistsOfBaseSarefDeviceTypeSarefSwitchMultipleSarefSwitch.String()
	ret += res.propSarefControlsPropertyBaseSarefPropertyTypeSarefPropertyMultipleSarefProperty.String()
	ret += res.propSarefHasProfileBaseSarefProfileTypeSarefProfileMultipleSarefProfile.String()
	ret += res.propSarefHasStateBaseSarefStateTypeSarefOpenCloseStateMultipleSarefOpenCloseState.String()
	ret += res.propSarefHasTypicalConsumptionBaseSarefPropertyTypeSarefPropertyMultipleSarefEnergySarefPower.String()
	ret += res.propSarefIsUsedForBaseSarefCommodityTypeSarefCommodityMultipleSarefCommodity.String()
	ret += res.propSarefMakesMeasurementBaseSarefMeasurementTypeSarefMeasurementMultipleSarefMeasurement.String()
	ret += res.propSarefMeasuresPropertyBaseSarefPropertyTypeSarefPropertyMultipleSarefProperty.String()
	ret += res.propSarefOffersBaseSarefServiceTypeSarefServiceMultipleSarefService.String()
	ret += res.propSarefAccomplishesBaseSarefTaskTypeSarefTaskMultipleSarefTask.String()
	ret += res.propSarefHasFunctionBaseSarefFunctionTypeSarefOpenCloseFunctionMultipleSarefOpenCloseFunction.String()
	ret += res.propSarefHasDescriptionBasestringTypestringSinglestring.String()
	ret += res.propSarefHasManufacturerBasestringTypestringSinglestring.String()
	ret += res.propSarefHasModelBasestringTypestringSinglestring.String()
	return
}


