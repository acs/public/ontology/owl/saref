/*
This file is auto-generated by OWL2Go (https://git.rwth-aachen.de/acs/public/ontology/owl/owl2go).

Copyright 2020 Institute for Automation of Complex Power Systems,
E.ON Energy Research Center, RWTH Aachen University

This project is licensed under either of
- Apache License, Version 2.0
- MIT License
at your option.

Apache License, Version 2.0:

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

MIT License:

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
*/

package ontology

import (
	"errors"
	"git.rwth-aachen.de/acs/public/ontology/owl/owl2go/pkg/rdf"
	"git.rwth-aachen.de/acs/public/ontology/owl/owl2go/pkg/owl"
	"strings"
)

// SarefProperty Anything that can be sensed, measured or controlled in households, common public buildings or offices. We propose here a list of properties that are relevant for the purpose of SAREF, but this list can be extended.
type SarefProperty interface {
	owl.Thing
	sarefIsControlledByDeviceMultipleSarefDevice
	sarefIsMeasuredByDeviceMultipleSarefDevice
	sarefRelatesToMeasurementMultipleSarefMeasurement
	IsSarefProperty() bool // indicates base class
}

// sSarefProperty Anything that can be sensed, measured or controlled in households, common public buildings or offices. We propose here a list of properties that are relevant for the purpose of SAREF, but this list can be extended.
type sSarefProperty struct {
	propCommon
	propSarefIsControlledByDeviceBaseSarefDeviceTypeSarefDeviceMultipleSarefDevice
	propSarefIsMeasuredByDeviceBaseSarefDeviceTypeSarefDeviceMultipleSarefDevice
	propSarefRelatesToMeasurementBaseSarefMeasurementTypeSarefMeasurementMultipleSarefMeasurement
}

// NewSarefProperty creates a new SarefProperty
func (mod *Model) NewSarefProperty(iri string) (ret SarefProperty, err error) {
	if mod.Exist(iri) {
		err = errors.New("Resource already exists")
		return
	}
	res := &sSarefProperty{}
	pc := propCommon{iri: iri, typ: "SarefProperty", model: mod}
	res.propCommon = pc
	mod.addSarefProperty(res)
	res.makeMaps()
	ret = res
	return
}

// makeMaps creates initiliazes maps
func (res *sSarefProperty) makeMaps() {
	res.sarefIsControlledByDevice = make(map[string]SarefDevice)
	res.sarefIsMeasuredByDevice = make(map[string]SarefDevice)
	res.sarefRelatesToMeasurement = make(map[string]SarefMeasurement)
	return
}

// addSarefProperty adds SarefProperty to model
func (mod *Model) addSarefProperty(res SarefProperty) {
	mod.mSarefProperty[res.IRI()] = res
	mod.mThing[res.IRI()] = res
	return
}

// SarefProperty returns all resources with given prefix
func (mod *Model) SarefProperty(prefix string) (res []SarefProperty) {
	for i := range mod.mSarefProperty {
		if strings.HasPrefix(i, prefix) {
			res = append(res, mod.mSarefProperty[i])
		}
	}
	return
}

// RemoveObject deletes all its references in this object
func (res *sSarefProperty) RemoveObject(obj owl.Thing, prop string) {
	switch prop {
	case "https://w3id.org/saref#isControlledByDevice":
		res.propSarefIsControlledByDeviceBaseSarefDeviceTypeSarefDeviceMultipleSarefDevice.removeObject(obj)
	case "https://w3id.org/saref#isMeasuredByDevice":
		res.propSarefIsMeasuredByDeviceBaseSarefDeviceTypeSarefDeviceMultipleSarefDevice.removeObject(obj)
	case "https://w3id.org/saref#relatesToMeasurement":
		res.propSarefRelatesToMeasurementBaseSarefMeasurementTypeSarefMeasurementMultipleSarefMeasurement.removeObject(obj)
	default:
	}
	return
}

// isSarefProperty indicates base class
func (res *sSarefProperty) IsSarefProperty() bool {
	return true
}

// InitFromNode initializes the resource from a graph node
func (res *sSarefProperty) InitFromNode(node *rdf.Node) (err error) {
	for i := range node.Edge {
		res.propsInit(node.Edge[i])
	}
	return
}

// propsInit initializes the property from a graph node
func (res *sSarefProperty) propsInit(pred *rdf.Edge) (err error) {
	switch pred.Pred.String() {
	case "https://w3id.org/saref#isControlledByDevice":
		res.propSarefIsControlledByDeviceBaseSarefDeviceTypeSarefDeviceMultipleSarefDevice.init(res.model, pred.Object.Term.String())
	case "https://w3id.org/saref#isMeasuredByDevice":
		res.propSarefIsMeasuredByDeviceBaseSarefDeviceTypeSarefDeviceMultipleSarefDevice.init(res.model, pred.Object.Term.String())
	case "https://w3id.org/saref#relatesToMeasurement":
		res.propSarefRelatesToMeasurementBaseSarefMeasurementTypeSarefMeasurementMultipleSarefMeasurement.init(res.model, pred.Object.Term.String())
	default:
	}
	return
}

// ToGraph creates a new owl graph node and adds it to the graph
func (res *sSarefProperty) ToGraph(g *rdf.Graph) {
	node := owl.AddObjectToGraph(g, "https://w3id.org/saref#Property", res)
	res.propsToGraph(node, g)
	return
}

// propsToGraph adds all properties to the graph
func (res *sSarefProperty) propsToGraph(node *rdf.Node, g *rdf.Graph) {
	res.propSarefIsControlledByDeviceBaseSarefDeviceTypeSarefDeviceMultipleSarefDevice.toGraph(node, g)
	res.propSarefIsMeasuredByDeviceBaseSarefDeviceTypeSarefDeviceMultipleSarefDevice.toGraph(node, g)
	res.propSarefRelatesToMeasurementBaseSarefMeasurementTypeSarefMeasurementMultipleSarefMeasurement.toGraph(node, g)
	return
}

// String prints the object into a string
func (res *sSarefProperty) String() (ret string) {
	ret = res.iri + " " + "SarefProperty\n"
	ret += res.propsString()
	return
}

// propsString prints the object into a string
func (res *sSarefProperty) propsString() (ret string) {
	ret = ""
	ret += res.propSarefIsControlledByDeviceBaseSarefDeviceTypeSarefDeviceMultipleSarefDevice.String()
	ret += res.propSarefIsMeasuredByDeviceBaseSarefDeviceTypeSarefDeviceMultipleSarefDevice.String()
	ret += res.propSarefRelatesToMeasurementBaseSarefMeasurementTypeSarefMeasurementMultipleSarefMeasurement.String()
	return
}


