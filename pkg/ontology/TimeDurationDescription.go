/*
This file is auto-generated by OWL2Go (https://git.rwth-aachen.de/acs/public/ontology/owl/owl2go).

Copyright 2020 Institute for Automation of Complex Power Systems,
E.ON Energy Research Center, RWTH Aachen University

This project is licensed under either of
- Apache License, Version 2.0
- MIT License
at your option.

Apache License, Version 2.0:

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

MIT License:

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
*/

package ontology

import (
	"errors"
	"git.rwth-aachen.de/acs/public/ontology/owl/owl2go/pkg/rdf"
	"git.rwth-aachen.de/acs/public/ontology/owl/owl2go/pkg/owl"
	"strings"
)

// TimeDurationDescription Description of temporal extent structured with separate values for the various elements of a calendar-clock system. The temporal reference system is fixed to Gregorian Calendar, and the range of each of the numeric properties is restricted to xsd:decimal
type TimeDurationDescription interface {
	TimeGeneralDurationDescription
	IsTimeDurationDescription() bool // indicates base class
}

// sTimeDurationDescription Description of temporal extent structured with separate values for the various elements of a calendar-clock system. The temporal reference system is fixed to Gregorian Calendar, and the range of each of the numeric properties is restricted to xsd:decimal
type sTimeDurationDescription struct {
	sTimeGeneralDurationDescription
}

// NewTimeDurationDescription creates a new TimeDurationDescription
func (mod *Model) NewTimeDurationDescription(iri string) (ret TimeDurationDescription, err error) {
	if mod.Exist(iri) {
		err = errors.New("Resource already exists")
		return
	}
	res := &sTimeDurationDescription{}
	pc := propCommon{iri: iri, typ: "TimeDurationDescription", model: mod}
	res.propCommon = pc
	mod.addTimeDurationDescription(res)
	res.makeMaps()
	ret = res
	return
}

// addTimeDurationDescription adds TimeDurationDescription to model
func (mod *Model) addTimeDurationDescription(res TimeDurationDescription) {
	mod.mTimeDurationDescription[res.IRI()] = res
	mod.addTimeGeneralDurationDescription(res)
	return
}

// TimeDurationDescription returns all resources with given prefix
func (mod *Model) TimeDurationDescription(prefix string) (res []TimeDurationDescription) {
	for i := range mod.mTimeDurationDescription {
		if strings.HasPrefix(i, prefix) {
			res = append(res, mod.mTimeDurationDescription[i])
		}
	}
	return
}

// isTimeDurationDescription indicates base class
func (res *sTimeDurationDescription) IsTimeDurationDescription() bool {
	return true
}

// ToGraph creates a new owl graph node and adds it to the graph
func (res *sTimeDurationDescription) ToGraph(g *rdf.Graph) {
	node := owl.AddObjectToGraph(g, "http://www.w3.org/2006/time#DurationDescription", res)
	res.propsToGraph(node, g)
	return
}

// String prints the object into a string
func (res *sTimeDurationDescription) String() (ret string) {
	ret = res.iri + " " + "TimeDurationDescription\n"
	ret += res.propsString()
	return
}


